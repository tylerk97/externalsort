// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)

import java.lang.Comparable;
import java.math.*;
/**
 * 
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.30.2020>
 */
public class MinHeap<E extends Comparable<? super E>> {
    
    private E[] Heap;
    private int size;
    private int n;

    public MinHeap(E[] h, int num, int max) {
        Heap = h;
        n = num;
        size = max;
    }
    
    public int heapsize() {
        return n;
    }
    
    public boolean isLeaf(int pos) {
        return (pos >= n/2) && (pos < n);
    }
    
    public int leftChild(int pos) {
        assert pos < n/2 : "Position has no left child";
        return 2 * pos + 1;
    }
    public int rightChild(int pos) {
        assert pos < (n-1)/2 : "Position has no right child";
        return 2*pos + 2;
    }
    public int parent(int pos) {
        assert pos > 0 : "Position has no parent";
        return (pos-1) / 2;
    }
    public void buildheap()
    { for (int i=n/2-1; i>=0; i--) siftdown(i); }

  /** Insert into heap */
  public void insert(E val) {
    assert n < size : "Heap is full";
    int curr = n++;
    Heap[curr] = val;                 // Start at end of heap
    // Now sift up until curr's parent's key > curr's key
    while ((curr != 0)  &&
           (Heap[curr].compareTo(Heap[parent(curr)]) < 0)) {
      DSutil.swap(Heap, curr, parent(curr));
      curr = parent(curr);
    }
  }
  /** Put element in its correct place */
  private void siftdown(int pos) {
    assert (pos >= 0) && (pos < n) : "Illegal heap position";
    while (!isLeaf(pos)) {
      int j = leftChild(pos);
      if ((j<(n-1)) && (Heap[j].compareTo(Heap[j+1]) > 0)) 
        j++; // j is now index of child with greater value
      if (Heap[pos].compareTo(Heap[j]) <= 0)
        return;
      DSutil.swap(Heap, pos, j);
      pos = j;  // Move down
    }
  }

  public E removemin() {     // Remove minimum value
    assert n > 0 : "Removing from empty heap";
    DSutil.swap(Heap, 0, --n); // Swap minimum with last value
    if (n != 0)      // Not on last element
      siftdown(0);   // Put new heap root val in correct place
    return Heap[n];
  }

  /** Remove element at specified position */
  public E remove(int pos) {
    assert (pos >= 0) && (pos < n) : "Illegal heap position";
    if (pos == (n-1)) n--; // Last element, no work to be done
    else
    {
      DSutil.swap(Heap, pos, --n); // Swap with last value
      // If we just swapped in a small value, push it up
      while ((pos > 0) && (Heap[pos].compareTo(Heap[parent(pos)]) < 0)) {
        DSutil.swap(Heap, pos, parent(pos));
        pos = parent(pos);
      }
      if (n != 0) siftdown(pos);   // If it is big, push down
    }
    return Heap[n];
  }
}
