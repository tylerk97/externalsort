import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)

/**
 * 
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.30.2020>
 */
public class GenFile {

    static final int BLOCK_SIZE = 8192;

    static final int NUM_REC = 8;

    static private Random value = new Random();


    /**
     * 
     *
     * @param args
     *            inputs
     * @throws IOException
     *             exceptions
     */
    public static void main(String[] args) throws IOException {
        short val;

        int filesize = Integer.parseInt(args[2]); // Size of file in blocks
        DataOutputStream file = new DataOutputStream(new BufferedOutputStream(
            new FileOutputStream(args[1])));

        // random numbers
        if (args[0].charAt(1) == 'b') {
            for (int i = 0; i < filesize; i++) {
                for (int j = 0; j < NUM_REC; j++) {
                    val = (short)(Math.abs(value.nextInt()) % 29999 + 1);
                    file.writeShort(val);
                }
            }
        }

        // ASCII version
        else if (args[0].charAt(1) == 'a') {
            for (int i = 0; i < filesize; i++) {
                for (int j = 0; j < NUM_REC; j++) {
                    if ((j % 2) == 1) {
                        val = (short)(8224);
                    }
                    else {
                        val = (short)(Math.abs(value.nextInt()) % 26 + 0x2041);
                    }
                    file.writeShort(val);
                }
            }
        }
        file.flush();
        file.close();
    }

}
