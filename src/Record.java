// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)

/**
 * 
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.30.2020>
 */
public class Record {

    private long recordID;
    private double key;


    /**
     * Constructor for a single record.
     * 
     * @param recordID2
     *            the recordID
     * @param key2
     *            the key
     */
    public Record(long recordID2, double key2) {
        this.recordID = recordID2;
        this.key = key2;
    }


    /**
     * Getter for recordID.
     * 
     * @return the recordID
     */
    public long getRecordID() {
        return recordID;
    }


    /**
     * Getter for key
     * 
     * @return the key
     */
    public double getKey() {
        return key;
    }


    /**
     * Setter for recordID
     * 
     * @param recordID2
     *            the new recordID
     */
    public void setRecordID(long recordID2) {
        recordID2 = recordID;
    }


    /**
     * Setter for key
     * 
     * @param key2
     *            the new key
     */
    public void setKey(double key2) {
        key2 = key;
    }

}
